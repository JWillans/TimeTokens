<?php

namespace JWillans\TimeTokens;

use JWillans\TimeTokens\Exception\TimeTokenException;

class TokenInfo
{

    protected string $token;

    protected \DateTimeImmutable $created;

    protected \DateTimeImmutable $now;

    protected \DateTimeImmutable $expires;

    protected string $hash;

    protected bool $expired;

    protected ?int $secondsRemaining;

    /**
     * @param string $token
     * @param \DateTimeImmutable $created
     * @param \DateTimeImmutable $now
     * @param \DateTimeImmutable $expires
     * @param string $hash
     * @throws TimeTokenException
     */
    public function __construct(string $token, \DateTimeImmutable $created, \DateTimeImmutable $now, \DateTimeImmutable $expires, string $hash)
    {
        $this->token = $token;
        $this->created = $created;
        $this->now = $now;
        $this->expires = $expires;
        $this->hash = $hash;
        $this->expired = $now >= $expires;

        $this->secondsRemaining = $this->expired ? null : self::transformDateIntervalToSeconds($now->diff($expires));
    }

    /**
     * @param \DateInterval $interval
     * @return int
     * @throws TimeTokenException
     */
    public static function transformDateIntervalToSeconds(\DateInterval $interval): int
    {
        if($interval->m > 0 || $interval->y > 0){
            throw new TimeTokenException('Interval must be less than one month');
        }
        return $interval->s +
            ($interval->i * 60) +
            ($interval->h * 3600) +
            ($interval->d * 86400)
            ;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreated(): \DateTimeImmutable
    {
        return $this->created;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getNow(): \DateTimeImmutable
    {
        return $this->now;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getExpires(): \DateTimeImmutable
    {
        return $this->expires;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->expired;
    }

    /**
     * @return int|null
     */
    public function getSecondsRemaining(): ?int
    {
        return $this->secondsRemaining;
    }

}