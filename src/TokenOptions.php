<?php

namespace JWillans\TimeTokens;

use JWillans\TimeTokens\Exception\TimeTokenException;

class TokenOptions
{

    protected \DateTimeImmutable $now;

    protected int $lifetimeSeconds = 60;

    public function __construct()
    {
        $this->now = new \DateTimeImmutable('now');
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getNow(): \DateTimeImmutable
    {
        return $this->now;
    }

    /**
     * @param \DateTimeImmutable $now
     */
    public function setNow(\DateTimeImmutable $now): void
    {
        $this->now = $now;
    }

    /**
     * @return int
     */
    public function getLifetimeSeconds(): int
    {
        return $this->lifetimeSeconds;
    }

    /**
     * @param int $lifetimeSeconds
     */
    public function setLifetimeSeconds(int $lifetimeSeconds): void
    {
        $this->lifetimeSeconds = $lifetimeSeconds;
    }

    /**
     * @return \DateInterval
     * @throws TimeTokenException
     */
    public function getLifetimeInterval(): \DateInterval
    {
        try{
            return new \DateInterval(sprintf('PT%sS', $this->lifetimeSeconds));
        }
        catch(\Exception $e){
            throw new TimeTokenException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @return string
     */
    public function getSignature(): string
    {
        return hash('sha256', serialize([
            $this->lifetimeSeconds,
        ]));
    }

}