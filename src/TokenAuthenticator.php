<?php

namespace JWillans\TimeTokens;

use JWillans\TimeTokens\Exception\ExpiredTokenException;
use JWillans\TimeTokens\Exception\TimeTokenException;
use JWillans\TimeTokens\Exception\InvalidTokenException;
use JWillans\TimeTokens\Exception\MalformedTokenException;

class TokenAuthenticator
{

    protected string $secret;

    const REGEX_TOKEN = '/^T-(?<year>[0-9]{4})(?<month>[0-9]{2})(?<day>[0-9]{2})(?<hour>[0-9]{2})(?<minute>[0-9]{2})(?<second>[0-9]{2})\-(?<hash>.+)$/';

    /**
     * @param string $secret
     */
    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }

    public function generate(?TokenOptions $options = null): string
    {
        $options = $options ?? new TokenOptions();
        $datetime = $options->getNow();
        $hash = $this->generateHash($datetime, $options);
        return sprintf(
            'T-%s-%s',
            $datetime->format('YmdHis'),
            $hash
        );
    }



    /**
     * @param string $token
     * @param TokenOptions $options
     * @return TokenInfo
     * @throws TimeTokenException
     * @throws MalformedTokenException
     */
    public static function parseToken(string $token, TokenOptions $options): TokenInfo
    {
        try{
            if(!preg_match(self::REGEX_TOKEN, $token, $match)){
                throw new MalformedTokenException('Token format is invalid');
            }

            $createdString = sprintf(
                '%s-%s-%sT%s:%s:%s%s',
                $match['year'],
                $match['month'],
                $match['day'],
                $match['hour'],
                $match['minute'],
                $match['second'],
                $options->getNow()->format('P')
            );

            $created = new \DateTimeImmutable($createdString);
            $expires = $created->add($options->getLifetimeInterval());
            $expired = $options->getNow() >= $expires;

            return new TokenInfo($token, $created, $options->getNow(), $expires, $match['hash']);
        }
        catch(\Exception $e){
            if($e instanceof TimeTokenException) throw $e;
            throw new TimeTokenException($e->getMessage(), 0, $e);
        }
    }

    protected function generateHash(\DateTimeImmutable $datetime, TokenOptions $options): string
    {
        $parts = [$datetime->format('c'), $this->secret, $options->getSignature()];
        return hash('sha256', serialize($parts));
    }

    /**
     * @param string $token
     * @param TokenOptions|null $options
     * @throws InvalidTokenException|TimeTokenException
     * @return true
     */
    public function validate(string $token, ?TokenOptions $options = null): bool
    {
        $options = $options ?? new TokenOptions();
        $info = self::parseToken($token, $options);
        if($info->isExpired()){
            throw new ExpiredTokenException(sprintf('Token expired %s', $info->getExpires()->format('c')));
        }
        $hash = $this->generateHash($info->getCreated(), $options);
        if($hash !== $info->getHash()) throw new InvalidTokenException('Invalid token (hash mismatch)');

        return true;
    }

    /**
     * @param string $token
     * @param TokenOptions|null $options
     * @return bool
     * @throws TimeTokenException
     */
    public function isValid(string $token, ?TokenOptions $options = null): bool
    {
        try{
            $this->validate($token, $options);
            return true;
        }
        catch(InvalidTokenException|ExpiredTokenException $e){
            return false;
        }
    }

}