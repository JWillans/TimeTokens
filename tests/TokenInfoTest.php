<?php

namespace JWillans\TimeTokens\Tests;

use JWillans\TimeTokens\Exception\TimeTokenException;
use JWillans\TimeTokens\TokenInfo;
use PHPUnit\Framework\TestCase;

class TokenInfoTest extends TestCase
{

    /**
     * @throws TimeTokenException
     */
    public function testTransformDateIntervalToSeconds(): void
    {
        $this->assertEquals(10, TokenInfo::transformDateIntervalToSeconds(new \DateInterval('PT10S')));
        $this->assertEquals(60, TokenInfo::transformDateIntervalToSeconds(new \DateInterval('PT1M')));
        $this->assertEquals(3600, TokenInfo::transformDateIntervalToSeconds(new \DateInterval('PT1H')));
        $this->assertEquals(86400, TokenInfo::transformDateIntervalToSeconds(new \DateInterval('P1D')));
    }

    /**
     * @throws TimeTokenException
     */
    public function testTransformDateIntervalToSecondsLimit(): void
    {
        $this->expectException(TimeTokenException::class);
        $this->expectExceptionMessage('Interval must be less than one month');
        TokenInfo::transformDateIntervalToSeconds(new \DateInterval('P1M'));
    }

}