<?php

namespace JWillans\TimeTokens\Tests;

use JWillans\TimeTokens\Exception\ExpiredTokenException;
use JWillans\TimeTokens\Exception\InvalidTokenException;
use JWillans\TimeTokens\Exception\TimeTokenException;
use JWillans\TimeTokens\TokenAuthenticator;
use JWillans\TimeTokens\TokenOptions;
use PHPUnit\Framework\TestCase;

class TokenAuthenticatorTest extends TestCase
{

    public function testTokenRegex(): void
    {
        $matched = preg_match(TokenAuthenticator::REGEX_TOKEN, 'T-20220103094212-ABCDEFGH', $match);
        $this->assertEquals(1, $matched);

        $this->assertEquals('2022', $match['year']);
        $this->assertEquals('01', $match['month']);
        $this->assertEquals('03', $match['day']);

        $this->assertEquals('09', $match['hour']);
        $this->assertEquals('42', $match['minute']);
        $this->assertEquals('12', $match['second']);

        $this->assertEquals('ABCDEFGH', $match['hash']);
    }



    public function testParseToken(): void
    {
        $now = new \DateTimeImmutable('2022-01-01T09:00:00+02:00');

        $options = new TokenOptions();
        $options->setNow($now);
        $options->setLifetimeSeconds(30);

        $info = TokenAuthenticator::parseToken('T-20220101090015-ABCDEFGH', $options);

        $this->assertEquals('T-20220101090015-ABCDEFGH', $info->getToken());

        $this->assertEquals('2022-01-01T09:00:15+02:00', $info->getCreated()->format('c'));
        $this->assertEquals('2022-01-01T09:00:00+02:00', $info->getNow()->format('c'));
        $this->assertEquals('2022-01-01T09:00:45+02:00', $info->getExpires()->format('c'));
        $this->assertEquals(45, $info->getSecondsRemaining());
        $this->assertEquals(false, $info->isExpired());
        $this->assertEquals('ABCDEFGH', $info->getHash());

        $now = new \DateTimeImmutable('2022-01-01T09:05:00+02:00');
        $options->setNow($now);

        $info = TokenAuthenticator::parseToken('T-20220101090015-ABCDEFGH', $options);

        $this->assertEquals('T-20220101090015-ABCDEFGH', $info->getToken());
        $this->assertEquals('2022-01-01T09:00:15+02:00', $info->getCreated()->format('c'));
        $this->assertEquals('2022-01-01T09:05:00+02:00', $info->getNow()->format('c'));
        $this->assertEquals('2022-01-01T09:00:45+02:00', $info->getExpires()->format('c'));
        $this->assertEquals(null, $info->getSecondsRemaining());
        $this->assertEquals(true, $info->isExpired());
        $this->assertEquals('ABCDEFGH', $info->getHash());
    }

    public function testGenerate(): void
    {

        $auth = new TokenAuthenticator('ABCDE');

        $options = new TokenOptions();
        $options->setLifetimeSeconds(30);
        $options->setNow(new \DateTimeImmutable('2022-01-01T09:00:00+02:00'));

        $this->assertEquals(
            'T-20220101090000-acd4729774b4b0f4d106bbe320e30df2d973045927b5036c8132a6faa0eeb0cd',
            $auth->generate($options)
        );

        $options->setNow(new \DateTimeImmutable('2022-05-03T06:12:34+00:00'));

        $this->assertEquals(
            'T-20220503061234-db8510b20665cf49defe9da22d8de417c5a105d31f354d4f318b535a83a10af1',
            $auth->generate($options)
        );

    }

    public function testValidatePass(): void
    {
        $token = 'T-20220503061234-db8510b20665cf49defe9da22d8de417c5a105d31f354d4f318b535a83a10af1';

        $now = new \DateTimeImmutable('2022-05-03T06:12:34+00:00');
        $options = new TokenOptions();
        $options->setNow($now);
        $options->setLifetimeSeconds(30);

        $auth = new TokenAuthenticator('ABCDE');

        $this->assertEquals(true, $auth->validate($token, $options));
    }

    public function testValidateFailSignature(): void
    {
        $token = 'T-20220503061234-db8510b20665cf49defe9da22d8de417c5a105d31f354d4f318b535a83a10af1';

        $now = new \DateTimeImmutable('2022-05-03T06:12:34+00:00');
        $options = new TokenOptions();
        $options->setNow($now);
        $options->setLifetimeSeconds(31);

        $auth = new TokenAuthenticator('ABCDE');

        $this->expectException(InvalidTokenException::class);
        $this->expectExceptionMessage('Invalid token (hash mismatch)');

        $auth->validate($token, $options);
    }

    public function testValidateFailExpired(): void
    {
        $token = 'T-20220503061234-db8510b20665cf49defe9da22d8de417c5a105d31f354d4f318b535a83a10af1';

        $now = new \DateTimeImmutable('2022-05-03T06:15:34+00:00');
        $options = new TokenOptions();
        $options->setNow($now);
        $options->setLifetimeSeconds(30);

        $auth = new TokenAuthenticator('ABCDE');

        $this->expectException(ExpiredTokenException::class);
        $this->expectExceptionMessage('Token expired 2022-05-03T06:13:04+00:00');

        $auth->validate($token, $options);
    }

}
