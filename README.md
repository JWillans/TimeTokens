# TimeTokens

A simple library for generating time based tokens

## Usage Example

### Token Creation

```php
$auth = new TokenAuthenticator('this-is-my-secret');
$token = $auth->generate();
```

### Token Creation (with options)

```php
$auth = new TokenAuthenticator('this-is-my-secret');
$options = new TokenOptions();
$options->setLifetimeSeconds(3600);
$token = $auth->generate($options);
```

### Token Validation

```php
$auth = new TokenAuthenticator('this-is-my-secret');
$auth->validate($token);

// or

if($auth->isValid($token)){
 // Do something
}
```

### Token Validation (with options)

```php
$auth = new TokenAuthenticator('this-is-my-secret');
$options = new TokenOptions();
$options->setLifetimeSeconds(3600);
$auth->validate($token, $options);

// or

if($auth->isValid($token, $options)){
 // Do something
}
```
